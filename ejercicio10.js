var i;
var num;
var n = 1;
var lista = [];
var min = 0;
var max = 0;
var media = 0;
var total = 0;

do {
    for (i = 0; i <= n; i++) {
        num = Number(prompt("Escriba un número", ""));
        if (num <= 0) {
            break;
        } else {
            lista[i] = num;
            n++;
            total += lista[i];
        }
    }
    min = Math.min.apply(null, lista);
    max = Math.max.apply(null, lista);
    media = total / lista.length;
} while (num <= 0);
console.log('El número mayor es ' + max + ', el menor es ' + min + ', la media es ' + media);