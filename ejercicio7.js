var anyo = Number(prompt("Escriba un año", ""));

bisiesto();

function bisiesto() {
    if ((anyo % 4 === 0) && (anyo % 100 !== 0)) {
        console.log('El año ' + anyo + ' es bisiesto');
    } else if (anyo % 400 === 0) {
        console.log('El año ' + anyo + ' es bisiesto');
    } else {
        console.log('El año ' + anyo + ' no es bisiesto');
    }
}