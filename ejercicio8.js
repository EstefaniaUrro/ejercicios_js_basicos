var i;
var num;
var cuadrado = 0;
var n = 1;

while (true) {
  num = Number(prompt("Escriba un número", ""));
  if (num < 10) {
    cuadrado = Math.pow(num, 2);
    console.log('El cuadrado de X ' + num + ' es ' + cuadrado);
  } else {
    console.log('Número incorrecto, entrar de nuevo');
  }
}